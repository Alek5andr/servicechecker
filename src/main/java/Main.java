import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Callback;
import org.apache.log4j.Logger;

import java.io.File;
import java.util.ArrayList;
import java.util.Timer;


/**
 * Created by aleksandr.govorkov on 07-Feb-18.
 */
public class Main extends Application {
    private static Logger logger = Logger.getRootLogger();

    private AlertBox alertBox = new AlertBox();
    private Timer mainTimer = null;
    private ObservableList<IPAddress> listOfIPAddresses = FXCollections.observableArrayList();
    private TableView<IPAddress> table = new TableView<>();
    private FileProcessor fp = null;
    private IPAddressStringValidation ipAddressStringValidation = new IPAddressStringValidation();

    public static void main (String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) {
        GridPane gp = new GridPane();
        Button addBtn = new Button("Add");
        Button uploadBtn = new Button("Upload TXT-file with IP-Address(es)");
        Button startBtn = new Button("Start");
        Button stopBtn = new Button("Stop");
        Button closeBtn = new Button("Close program");
        Label notificationsLbl = new Label("Notifications");
        TextField fromEMailField = new TextField();
        PasswordField passwordField = new PasswordField();
        TextField toEMailField = new TextField();
        Label ipAddressesToPingEveryLbl = new Label("IP-Address(es) to ping every");
        TextField numberField =  new TextField("5");
        RadioButton secondsRadioBtn =  new RadioButton("second(s)");
        RadioButton minutesRadioBtn =  new RadioButton("minute(s)");
        ArrayList<RadioButton> listOfRadioBtns = new ArrayList<>();
        ToggleGroup radioBtnsGroup = new ToggleGroup();
        TextField ipAddressField = new TextField();
        TableColumn<IPAddress, String> ipAddressColumn = new TableColumn<>();

        // Setting up the stage
        stage.setTitle("IP checker - by Alek5");
        stage.setOnCloseRequest(e -> {
            e.consume();
            closeProgram(stage, mainTimer);
        });

        // Setting up the grid
        gp.setPadding(new Insets(10, 10, 10, 10));
        gp.setVgap(8);
        gp.setHgap(10);
        gp.setGridLinesVisible(false);
        for (int i = 0; i < 2; i++) {
            ColumnConstraints column = new ColumnConstraints(400);
            gp.getColumnConstraints().add(column);
        }

        // Setting up elements (to the grid)
        // Setting up 'Notifications' label
        notificationsLbl.setFont(javafx.scene.text.Font.font("Verdana", FontWeight.BOLD, 20));
        GridPane.setConstraints(notificationsLbl, 0, 0);

        // Setting up 'From E-Mail' field
        fromEMailField.setFont(Font.font("Verdana", 18));
        fromEMailField.setPromptText("From E-Mail");
        fromEMailField.setPrefColumnCount(50);
        GridPane.setConstraints(fromEMailField, 0, 1);

        // Setting up 'Password' field
        passwordField.setFont(Font.font("Verdana", 18));
        passwordField.setPromptText("Password");
        passwordField.setPrefColumnCount(50);
        GridPane.setConstraints(passwordField, 1, 1);

        // Setting up 'To E-Mail' field
        toEMailField.setFont(Font.font("Verdana", 18));
        toEMailField.setPromptText("To E-Mail");
        toEMailField.setPrefColumnCount(100);
        GridPane.setConstraints(toEMailField, 0, 2);

        // Setting up 'ping string' label
        ipAddressesToPingEveryLbl.setFont(Font.font("Verdana", 18));
        GridPane.setConstraints(ipAddressesToPingEveryLbl, 0, 6);

        // Setting up 'number' field
        numberField.setFont(Font.font("Verdana", 18));
        GridPane.setConstraints(numberField, 1, 6);

        // Setting up radio buttons
        minutesRadioBtn.setFont(Font.font("Verdana", 18));
        minutesRadioBtn.setToggleGroup(radioBtnsGroup);
        secondsRadioBtn.setSelected(true);
        minutesRadioBtn.requestFocus();
        secondsRadioBtn.setFont(Font.font("Verdana", 18));
        secondsRadioBtn.setToggleGroup(radioBtnsGroup);
        listOfRadioBtns.add(minutesRadioBtn);
        listOfRadioBtns.add(secondsRadioBtn);
        HBox hbox = new HBox(minutesRadioBtn, secondsRadioBtn);
        hbox.setSpacing(20);
        GridPane.setConstraints(hbox, 1, 7);

        // Setting up 'IP-Address' field
        ipAddressField.setFont(Font.font("Verdana", 18));
        ipAddressField.setPromptText("IP-Address");
        GridPane.setConstraints(ipAddressField, 0, 8);

        // Setting up table of IP-Addresses
        ipAddressColumn.setMinWidth(193);
        ipAddressColumn.setResizable(false);
        ipAddressColumn.setCellValueFactory(new PropertyValueFactory<>("ipAddress"));
        table.getColumns().add(ipAddressColumn);
        addButtonToTable();

        // Setting up buttons
        addBtn.setFont(Font.font("Verdana", 18));
        GridPane.setConstraints(addBtn, 1, 8);
        GridPane.setConstraints(table, 0, 9);
        uploadBtn.setFont(Font.font("Verdana", 18));
        GridPane.setConstraints(uploadBtn, 1, 9);
        startBtn.setFont(Font.font("Verdana", 18));
        GridPane.setConstraints(startBtn, 1, 10);
        stopBtn.setFont(Font.font("Verdana", 18));
        stopBtn.setVisible(false);
        GridPane.setConstraints(stopBtn, 1, 10);
        closeBtn.setFont(Font.font("Verdana", 18));
        GridPane.setConstraints(closeBtn, 1, 0);

        addBtn.setOnAction(e -> populateIPAddressesList(table,ipAddressField));

        uploadBtn.setOnAction(e -> uploadFileWithIPAddresses(stage));

        startBtn.setOnAction(e -> startCheckingIPAddresses(
                fromEMailField,
                passwordField,
                toEMailField,
                numberField,
                ipAddressField,
                addBtn,
                startBtn,
                stopBtn,
                uploadBtn,
                listOfRadioBtns,
                radioBtnsGroup));

        stopBtn.setOnAction(e -> {
            mainTimer.cancel();
            enableAndSwitchSomeElements(fromEMailField, passwordField, toEMailField, numberField, listOfRadioBtns, ipAddressField, addBtn, table, startBtn, stopBtn, uploadBtn);
        });

        closeBtn.setOnAction(e -> closeProgram(stage, mainTimer));

        gp.getChildren().addAll(
                notificationsLbl,
                fromEMailField,
                passwordField,
                toEMailField,
                ipAddressesToPingEveryLbl,
                numberField,
                hbox,
                ipAddressField,
                addBtn,
                uploadBtn,
                startBtn,
                stopBtn,
                closeBtn,
                table);

        Scene scene = new Scene(gp, 830, 600);
        stage.setScene(scene);
        stage.show();
    }

    private void closeProgram(Stage stage, Timer timer) {
        if (timer != null) {
            timer.cancel();
        }
        stage.close();
    }

    private void populateIPAddressesList(TableView<IPAddress> table, TextField ipAddressField) {
        String ipAddress = ipAddressField.getText();

        if(ipAddressStringValidation.isIPAddressFieldEmpty(ipAddress)) {
            alertBox.display("Enter IP-Address");
        } else {
            if (ipAddressStringValidation.isIPAddressValid(ipAddress)) {
                composeTableOfIPAddresses(ipAddress);
                ipAddressField.clear();
            } else {
                alertBox.display("Enter valid IP-Address");
            }
        }
    }

    private void composeTableOfIPAddresses(String ipAddress) {
        listOfIPAddresses.add(new IPAddress(ipAddress));
        table.setItems(listOfIPAddresses);
    }

    private void disableAndSwitchSomeElements(
            TextField fromEMailField,
            PasswordField passwordField,
            TextField toEMailField,
            TextField numberField,
            ArrayList<RadioButton> listOfRadioBtns,
            TextField ipAddressField,
            Button addBtn,
            TableView<IPAddress> table,
            Button startBtn,
            Button stopBtn,
            Button uploadBtn) {
        boolean disable = true;

        fromEMailField.setDisable(disable);
        passwordField.setDisable(disable);
        toEMailField.setDisable(disable);
        numberField.setDisable(disable);
        ipAddressField.setDisable(disable);
        addBtn.setDisable(disable);
        table.setDisable(disable);
        uploadBtn.setDisable(disable);
        listOfRadioBtns.forEach(radioBtn -> radioBtn.setDisable(true));
        toggleStartAndStopBtns(startBtn, stopBtn);
    }

    private void toggleStartAndStopBtns(Button startBtn, Button stopBtn) {
        boolean visibility = false;

        visibility = startBtn.isVisible() ? false : true;
        startBtn.setVisible(visibility);
        stopBtn.setVisible(!visibility);
    }

    private void enableAndSwitchSomeElements(
            TextField fromEMailField,
            PasswordField passwordField,
            TextField toEMailField,
            TextField numberField,
            ArrayList<RadioButton> listOfRadioBtns,
            TextField ipAddressField,
            Button addBtn,
            TableView<IPAddress> table,
            Button startBtn,
            Button stopBtn, Button uploadBtn) {
        boolean disable = false;

        fromEMailField.setDisable(disable);
        passwordField.setDisable(disable);
        toEMailField.setDisable(disable);
        numberField.setDisable(disable);
        ipAddressField.setDisable(disable);
        addBtn.setDisable(disable);
        table.setDisable(disable);
        uploadBtn.setDisable(disable);
        listOfRadioBtns.forEach(radioBtn -> radioBtn.setDisable(disable));
        toggleStartAndStopBtns(startBtn, stopBtn);
    }

    private void startCheckingIPAddresses(
            TextField fromEMailField,
            PasswordField passwordField,
            TextField toEMailField,
            TextField numberField,
            TextField ipAddressField,
            Button addBtn,
            Button startBtn,
            Button stopBtn,
            Button uploadBtn,
            ArrayList<RadioButton> listOfRadioBtns,
            ToggleGroup radioBtnsTG) {
        CheckNotificationsFields cnf = new CheckNotificationsFields(fromEMailField, passwordField, toEMailField);
        if(cnf.isAnyNotificationFieldNull()) {
            alertBox.display("Some of notifications fields is/are empty. Fill all of them in.");
        } else {
            if (cnf.areEMailsValid()) {
                if (listOfIPAddresses.isEmpty()) {
                    alertBox.display("There is no any added IP-Address.");
                } else {
                    disableAndSwitchSomeElements(fromEMailField, passwordField, toEMailField, numberField, listOfRadioBtns, ipAddressField, addBtn, table, startBtn, stopBtn, uploadBtn);
                    long sleepMilliseconds = setSleepingCycle(numberField, radioBtnsTG);
                    Timer timer = new Timer();
                    timer.schedule(new ServiceChecker(listOfIPAddresses, fromEMailField.getText(), passwordField.getText(), toEMailField.getText()), 0, sleepMilliseconds);
                    mainTimer = timer;
                }
            } else {
                alertBox.display("Type valid e-mail address in.");
            }
        }
    }

    private long setSleepingCycle(TextField numberField, ToggleGroup radioBtnsTG) {
        long sleepNumber = Long.parseLong(numberField.getText());
        String selectedRadioBtn = null;

        if (radioBtnsTG.getSelectedToggle() != null) {
            RadioButton radiobutton = (RadioButton) radioBtnsTG.getSelectedToggle();
            selectedRadioBtn = radiobutton.getText();
        }
        sleepNumber = "second(s)".equals(selectedRadioBtn) ? sleepNumber * 1000:  sleepNumber * 1000 * 60;

        return sleepNumber;
    }

    private void addButtonToTable() {
        TableColumn<IPAddress, Void> deleteBtnColumn = new TableColumn();
        deleteBtnColumn.setMinWidth(192);
        deleteBtnColumn.setResizable(false);

        Callback<TableColumn<IPAddress, Void>, TableCell<IPAddress, Void>> cellFactory = new Callback<TableColumn<IPAddress, Void>, TableCell<IPAddress, Void>>() {
            @Override
            public TableCell<IPAddress, Void> call(final TableColumn<IPAddress, Void> param) {
                final TableCell<IPAddress, Void> cell = new TableCell<IPAddress, Void>() {

                    private final Button deleteBtn = new Button("Delete");
                    {
                        deleteBtn.setOnAction(e -> table.getItems().remove(getIndex()));
                    }

                    @Override
                    public void updateItem(Void item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            setGraphic(deleteBtn);
                        }
                    }
                };
                return cell;
            }
        };

        deleteBtnColumn.setCellFactory(cellFactory);
        table.getColumns().add(deleteBtnColumn);
    }

    private void uploadFileWithIPAddresses(Stage stage) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Upload TXT-file containing IP-Address(es)");
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("TXT File", "*.txt"));
        File file = fileChooser.showOpenDialog(stage);
        if (file != null) {
            fp = new FileProcessor(file);

            for (String ipAddress : fp.getListOfIPAddressesFromFile()) {
                composeTableOfIPAddresses(ipAddress);
            }
        }
    }

    public static void logError(Exception e) {
        logger.error("Runtime error occurred - ", e);
    }

    public static void logInfo(String info) {
        logger.info(info);
    }

}
