
/**
 * Created by aleksandr.govorkov on 09-Feb-18.
 */
public class IPAddress {
    private String ipAddress = null;

    public IPAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }
}
