import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.TimerTask;

/**
 * Created by aleksandr.govorkov on 07-Feb-18.
 */

public class ServiceChecker extends TimerTask {
    private ObservableList<IPAddress> addedIPAddresses = FXCollections.observableArrayList();
    private String fromEMail = null;
    private String password = null;
    private String toEMail = null;
    private int firstByte = 0;
    private int secondByte = 0;
    private int thirdByte = 0;
    private int fourthByte = 0;
    private MailSender ms = null;


    public ServiceChecker(ObservableList<IPAddress> addedIPAddresses, String fromEMail, String password, String toEMail) {
        this.addedIPAddresses = addedIPAddresses;
        this.fromEMail = fromEMail;
        this.password = password;
        this.toEMail = toEMail;
        ms = new MailSender(this.fromEMail, this.password, this.toEMail);
    }

    public void checkAddedIPAddresses() {
        ArrayList<String> unreachableHosts = new ArrayList<>();
        for (IPAddress addedIPAddress : addedIPAddresses) {
            String[] arrayOfBytes = addedIPAddress.getIpAddress().split("\\.");
            firstByte = Integer.parseInt(arrayOfBytes[0]);
            secondByte = Integer.parseInt(arrayOfBytes[1]);
            thirdByte = Integer.parseInt(arrayOfBytes[2]);
            fourthByte = Integer.parseInt(arrayOfBytes[3]);

            try {
                byte[] ipAddress = new byte[]{(byte) firstByte, (byte) secondByte, (byte) thirdByte, (byte) fourthByte};
                InetAddress inet = InetAddress.getByAddress(ipAddress);
                String ipAddressString = inet.getHostAddress();

                System.out.println("Sending Ping Request to " + ipAddressString);
                System.out.println(inet.isReachable(5000) ? "Host is reachable" : "Host is NOT reachable");
                if(!inet.isReachable(5000)) {
                    unreachableHosts.add(ipAddressString);
                }
            } catch (UnknownHostException e) {
                Main.logError(e);
            } catch (IOException e) {
                Main.logError(e);
            }
        }
        if (!unreachableHosts.isEmpty()) {
            ms.send(unreachableHosts);
        }
    }

    public void run() {
        try {
            checkAddedIPAddresses();
        } catch (ConcurrentModificationException e) {
            System.out.println("Caught ConcurrentModificationException");
        }
    }
}
