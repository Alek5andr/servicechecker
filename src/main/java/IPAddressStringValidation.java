import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by aleksandr.govorkov on 11-Feb-18.
 */
public class IPAddressStringValidation {

    public boolean isIPAddressFieldEmpty(String ipAddressString) {
        if ("".equals(ipAddressString)) {
            return true;
        }
        return false;
    }

    public boolean isIPAddressValid(String ipAddress) {
        boolean matches = false;
        String patternString = "^(?=.*[^.]$)((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.?){4}$";
        Pattern pattern = Pattern.compile(patternString);
        Matcher matcher = pattern.matcher(ipAddress);
        matches = matcher.matches();

        return matches;
    }
}
