import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * Created by aleksandr.govorkov on 08-Feb-18.
 */
public class AlertBox {
    private Stage alertStage;
    private Scene alertScene;
    private Label someFieldIsEmptyLbl;
    private Button closeBtn;

    public void display(String errorMessage) {
        display("", errorMessage);
    }

    public void display(String title, String errorMessage) {
        alertStage = new Stage();
        someFieldIsEmptyLbl = new Label();
        closeBtn = new Button("Close the window");

        alertStage.initModality(Modality.APPLICATION_MODAL);
        alertStage.setTitle(title);
        alertStage.setMinWidth(250);

        someFieldIsEmptyLbl.setFont(Font.font("Verdana", FontWeight.BOLD, FontPosture.ITALIC, 20));
        someFieldIsEmptyLbl.setTextFill(Color.RED);
        someFieldIsEmptyLbl.setText(errorMessage);
        closeBtn.setOnAction(e -> alertStage.close());

        VBox layout = new VBox(20);
        layout.getChildren().addAll(someFieldIsEmptyLbl, closeBtn);
        layout.setAlignment(Pos.CENTER);

        alertScene = new Scene(layout);
        alertStage.setScene(alertScene);
        alertStage.showAndWait();
    }
}
