import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;
import java.lang.StringBuilder;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class MailSender {

    private String fromEMail = null;
    private String password = null;
    private String toEMail = null;

    public MailSender(String fromEMail, String password, String toEMail) {
        this.fromEMail = fromEMail;
        this.password = password;
        this.toEMail = toEMail;
    }

    public void send(ArrayList<String> unreachableHosts) {
        StringBuilder builder = new StringBuilder(); // Create a new StringBuilder.
        String messageString = "Services are down:\n";

        for (String unreachableHost : unreachableHosts) {
            builder.append(unreachableHost).append("\n");
        }
        messageString = messageString + builder.toString();

        System.out.println("SSLEmail Start");
        Properties props = new Properties();
        props.put("mail.smtp.host", "192.168.219.244"); //SMTP Host
        props.put("mail.smtp.port", "25"); //SSL Port
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory"); //SSL Factory Class
        props.put("mail.smtp.auth", "true"); //enable authentication
        props.put("mail.smtp.starttls.enable", "true"); //enable STARTTLS

        //create Authenticator object to pass in Session.getInstance argument
        Authenticator auth = new Authenticator() {
            //override the getPasswordAuthentication method
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(fromEMail, password);
            }
        };
        Session session = Session.getInstance(props, auth);

        MimeMessage msg = new MimeMessage(session);
        try {
            //set message headers
            msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
            msg.addHeader("format", "flowed");
            msg.addHeader("Content-Transfer-Encoding", "8bit");

            msg.setFrom(new InternetAddress(fromEMail, "Auto IP-Checker"));
            msg.setSubject("EPAKP service(s) - DOWN", "UTF-8");
            msg.setText(messageString, "UTF-8");
            msg.setSentDate(new Date());
            msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEMail, false));
        } catch (MessagingException e) {
            Main.logError(e);
        } catch (UnsupportedEncodingException e) {
            Main.logError(e);
        }
        System.out.println("Message is ready");

        try {
            Transport.send(msg);
        } catch (MessagingException e) {
            Main.logError(e);
        }
        System.out.println("EMail Sent Successfully!!");
    }

}
