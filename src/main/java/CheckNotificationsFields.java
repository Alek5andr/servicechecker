import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

import java.util.ArrayList;
import java.util.Collection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by aleksandr.govorkov on 08-Feb-18.
 */
public class CheckNotificationsFields {
    private TextField fromEMailField;
    private PasswordField passwordField;
    private TextField toEMailField;
    private String fromEMailFieldString;
    private String toEMailFieldString;
    private ArrayList<String> emailsFieldsStrings = new ArrayList<String>();

    public CheckNotificationsFields(TextField fromEMailField, PasswordField passwordField, TextField toEMailField) {
        this.fromEMailField = fromEMailField;
        this.passwordField = passwordField;
        this.toEMailField = toEMailField;
        fromEMailFieldString = fromEMailField.getText();
        toEMailFieldString = toEMailField.getText();
        emailsFieldsStrings.add(fromEMailFieldString);
        emailsFieldsStrings.add(toEMailFieldString);
    }

    public boolean isAnyNotificationFieldNull() {
        if ("".equals(fromEMailFieldString) || "".equals(passwordField.getText()) || "".equals(toEMailFieldString)) {
            return true;
        }
        return false;
    }

    public boolean areEMailsValid() {
        String patternString = "(,? ?[a-z0-9_.]+@[a-z]+\\.(.+\\.)?[a-z]{2,3})+";
        Pattern pattern = Pattern.compile(patternString);
        boolean matches;

        for (int i = 0; i < emailsFieldsStrings.size(); i++) {
            Matcher matcher = pattern.matcher(emailsFieldsStrings.get(i));
            matches = matcher.matches();
            if (!matches) {
                return false;
            }
        }
        return true;
    }

}
