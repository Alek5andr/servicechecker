import java.awt.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by aleksandr.govorkov on 10-Feb-18.
 */
public class FileProcessor {
    private Desktop desktop = Desktop.getDesktop();
    private File file = null;
    private ArrayList<String> listOfIPAddresses = new ArrayList<>();
    private IPAddressStringValidation ipAddressStringValidation = new IPAddressStringValidation();
    private AlertBox alertBox = new AlertBox();

    public FileProcessor(File file) {
        this.file = file;
    }

    public void openFile() {
        try {
            desktop.open(file);
        } catch (IOException ex) {
            Main.logError(ex);
        }
    }

    public ArrayList<String> getListOfIPAddressesFromFile() {
        String ipAddress;

        try {
            boolean isLogExist = false;
            Scanner input = new Scanner(file);
            
            // read one line at a time
            while (input.hasNextLine()) // verify there are more lines
            // or will get an exception
            {
                ipAddress = input.nextLine();
                if (ipAddressStringValidation.isIPAddressValid(ipAddress)) {
                    listOfIPAddresses.add(ipAddress);
                } else {
                    isLogExist = true;
                    Main.logInfo("Invalid IP-address - " + ipAddress);
                }
            }
            // release resources to operating system when done
            input.close();
            if (isLogExist) {
                alertBox.display("Info","Log was created. See it FYI.");
            }
        } catch (FileNotFoundException e) {
            alertBox.display("File is not found");
        }
        return listOfIPAddresses;
    }
}
