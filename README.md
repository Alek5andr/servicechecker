# README #

### What is this repository for? ###

* This repository contains source files as well as bin file for Java desktop application, what can be used for checking accessibility of machines by their IP-address.
  * It took 4 hours of Friday to set up the repository, push right files and compose decent README. Jeez... that's how when you don't do much with Git =P **`Ofcourse this repository is for learning!`** 
* This tool was created specifically for personal needs. Nothing similar could be found within 4 hours of scouting in the Internet: `local executable for checking "Up" status of machines by IP-addresses`. See **Instruction** section below.
* You can use this tool by downloading it from _`out/artifacts/ServiceChecker_jar`_ directory.
  * Or you can clone the source code, edit it on your own by **keeping copyright to me** and compile your version of the tool.
* Created by Alek5andr.
  * If you wherever find my e-mail address, write me with suggestions or simple "Thanks!" X)

### IP checker ###

![Screenshot](images/ipchecker.PNG)

### Instruction (briefly) ###
* The Internet access or local network should be available.
* You can input IP-addresses one by one or upload `*.TXT` file with IP-addresses: each on 1 row.
* `*.log` file is being created in the same directory, where the tool was launched from. If there are some errors during the tool's execution, they will be written to this log-file.
  * What to do next with the log-file? Dunno... get rid of it. Or make an attempt to deliver it to me. I would be glad to fix bug(s).


### TO-DO ###
* Timer or some kind of notification, stating that check is being made and _NN_ time left to next check.
* Test the tool without Internet/network connection.
